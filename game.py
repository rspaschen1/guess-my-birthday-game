from random import randint

# Dictionary for month names
MONTH_NAMES = {
    1: "January",
    2: "February",
    3: "March",
    4: "April",
    5: "May",
    6: "June",
    7: "July",
    8: "August",
    9: "September",
    10: "October",
    11: "November",
    12: "December"
}

# Define variables to maintain minimum and maximum dates
minimum_date = None
maximum_date = None


class Birthday:
    # Generates month 1-12
    def __generate_month():
        return randint(1, 12)

    # Generates a random year through 1924-2004
    def __generate_year():
        return randint(minimum_date.year if minimum_date is not None else 1924,
                       maximum_date.year if maximum_date is not None else 2004)

    # Assigns generated month/year to object
    def __init__(self):
        self.month = Birthday.__generate_month()
        self.year = Birthday.__generate_year()

    # Checks for equality on birthday combination
    def __eq__(self, other):
        return self.month == other.month and self.year == other.year

    def __lt__(self, other):
        return self.year < other.year or (self.year == other.year and self.month < other.month)

    def __gt__(self, other):
        return self.year > other.year or (self.year == other.year and self.month > other.month)


def main():
    # Make sure global variables are recognized in this local function
    global minimum_date, maximum_date

    name = input("Hi! What is your name?: ")

    for i in range(1, 6):
        # Generate a valid birthday
        birthday = Birthday()
        while (maximum_date is not None and birthday > maximum_date) or (minimum_date is not None and birthday < minimum_date):
            birthday = Birthday()

        # Makes guess
        print(
            f'Guess {i}: {name}, were you born in {MONTH_NAMES[birthday.month]} {birthday.year}?')
        response = input(
            "If so, say yes, otherwise: is your birthday before or after? ")

        # Handles user response to guess
        if response.lower() == "yes":
            print("I guessed it!")
            return
        elif response.lower() == "before":
            if i < 5:
                maximum_date = birthday
                print("Drat! Lemme try again!")

        elif response.lower() == "after":
            if i < 5:
                minimum_date = birthday
                print("Drat! Lemme try again")

    print("I have other things to do. Good bye.")


main()
